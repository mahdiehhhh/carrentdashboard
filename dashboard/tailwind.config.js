/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily: {
      plusjakartasans: ["PlusJakartaSans"],
    },
    extend: {},
    colors: {
      primary: "#3563E9",
      secondary: "#1A202C",
      lightblue: "#90A3BF",
      darkblue: "#3D5278",
    },
  },
  plugins: [],
};
